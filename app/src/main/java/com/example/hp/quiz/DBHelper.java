package com.example.hp.quiz;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    private final Context fContext;
    private static DBHelper instance;
    private static final String DB_NAME = "quiz.db";
    private static final int DB_VERSION = 1;

    private DBHelper(Context context) {
        super(context.getApplicationContext(), DB_NAME, null, DB_VERSION);
        fContext = context;
    }

    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBSchema.Quiz.CREATE_TABLE_SQL);

        ContentValues cv = new ContentValues();

        Resources res = fContext.getResources();
        XmlResourceParser parser = res.getXml(R.xml.quiz);

        try {
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();
                if (name.equals("questions")) {
                    String question;
                    String firstAnswer;
                    String secondAnswer;
                    String thirdAnswer;
                    String fourthAnswer;
                    String rightAnswer;
                    while (parser.next() != XmlPullParser.END_TAG) {
                        if (parser.getEventType() != XmlPullParser.START_TAG) {
                            continue;
                        }
                        name = parser.getName();
                        switch (name) {
                            case "question":
                                question = readText(parser);
                                cv.put(DBSchema.Quiz.COLUMN_QUESTION, question);
                                break;
                            case "firstAnswer":
                                firstAnswer = readText(parser);
                                cv.put(DBSchema.Quiz.COLUMN_FIRST_ANSWER, firstAnswer);
                                break;
                            case "secondAnswer":
                                secondAnswer = readText(parser);
                                cv.put(DBSchema.Quiz.COLUMN_SECOND_ANSWER, secondAnswer);
                                break;
                            case "thirdAnswer":
                                thirdAnswer = readText(parser);
                                cv.put(DBSchema.Quiz.COLUMN_THIRD_ANSWER, thirdAnswer);
                                break;
                            case "fourthAnswer":
                                fourthAnswer = readText(parser);
                                cv.put(DBSchema.Quiz.COLUMN_FOURTH_ANSWER, fourthAnswer);
                                break;
                            case "rightAnswer":
                                rightAnswer = readText(parser);
                                cv.put(DBSchema.Quiz.COLUMN_RIGHT_ANSWER, Integer.parseInt(rightAnswer));
                                break;
                        }
                    }
                    db.insert(DBSchema.Quiz.TABLE_NAME, null, cv);
                }
            }

//            for (int i = 0; i < 50; i++) {
//                cv.clear();
//                cv.put(DBSchema.Quiz.COLUMN_QUESTION, "Question " + i);
//                cv.put(DBSchema.Quiz.COLUMN_FIRST_ANSWER, "firstAnswer " + i);
//                cv.put(DBSchema.Quiz.COLUMN_SECOND_ANSWER, "secondAnswer " + i);
//                cv.put(DBSchema.Quiz.COLUMN_THIRD_ANSWER, "thirdAnswer " + i);
//                cv.put(DBSchema.Quiz.COLUMN_FOURTH_ANSWER, "fourthAnswer " + i);
//                cv.put(DBSchema.Quiz.COLUMN_RIGHT_ANSWER, i % 4 + 1);
//                db.insert(DBSchema.Quiz.TABLE_NAME, null, cv);
//            }


        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
    }

    private String readText(XmlPullParser parser) throws IOException,
            XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

//    private void generateQuizList() {
//
//        for (int i = 0; i < 5; i++) {
//            QuizModel quizModel = new QuizModel();
//            quizModel.setQuestion("question" + i);
//            quizModel.setFirstAnswer("firstAnswer" + i);
//            quizModel.setSecondAnswer("secondAnswer" + i);
//            quizModel.setRightAnswer(i % 2);
//            addQuiz(quizModel);
//        }
//    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF IT EXISTS " + DB_NAME);
        onCreate(db);
    }

    public long addQuiz(QuizModel quizModel) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.clear();
        cv.put(DBSchema.Quiz.COLUMN_QUESTION, quizModel.getQuestion());
        cv.put(DBSchema.Quiz.COLUMN_FIRST_ANSWER, quizModel.getFirstAnswer());
        cv.put(DBSchema.Quiz.COLUMN_SECOND_ANSWER, quizModel.getSecondAnswer());
        cv.put(DBSchema.Quiz.COLUMN_THIRD_ANSWER, quizModel.getThirdAnswer());
        cv.put(DBSchema.Quiz.COLUMN_FOURTH_ANSWER, quizModel.getFourthAnswer());
        cv.put(DBSchema.Quiz.COLUMN_RIGHT_ANSWER, quizModel.getRightAnswer());

        long rowId = db.insert(DBSchema.Quiz.TABLE_NAME, null, cv);
        db.close();
        return rowId;
    }

    public int deleteQuiz(QuizModel quizModel) {
        SQLiteDatabase db = getWritableDatabase();

        int numberOfAffectedRows = db.delete(DBSchema.Quiz.TABLE_NAME, DBSchema.Quiz._ID + " = ?", new String[]{"" + quizModel.getId()});

        db.close();
        return numberOfAffectedRows;
    }

    public int updateQuiz(QuizModel quizModel) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DBSchema.Quiz.COLUMN_QUESTION, quizModel.getQuestion());
        cv.put(DBSchema.Quiz.COLUMN_FIRST_ANSWER, quizModel.getFirstAnswer());
        cv.put(DBSchema.Quiz.COLUMN_SECOND_ANSWER, quizModel.getSecondAnswer());
        cv.put(DBSchema.Quiz.COLUMN_THIRD_ANSWER, quizModel.getThirdAnswer());
        cv.put(DBSchema.Quiz.COLUMN_FOURTH_ANSWER, quizModel.getFourthAnswer());
        cv.put(DBSchema.Quiz.COLUMN_RIGHT_ANSWER, quizModel.getRightAnswer());

        int numberOfAffectedRows = db.update(DBSchema.Quiz.TABLE_NAME, cv, DBSchema.Quiz._ID + " = ?", new String[]{"" + quizModel.getId()});
        db.close();
        return numberOfAffectedRows;
    }

    public QuizModel getQuizById(long id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBSchema.Quiz.TABLE_NAME, null, DBSchema.Quiz._ID + " = ?", new String[]{"" + id}, null, null, null, null);
        QuizModel quiz = null;
        if (c.moveToFirst()) {
            quiz = getQuiz(c);
        }

        c.close();
        db.close();
        return quiz;
    }

    public List<QuizModel> getQuizList() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBSchema.Quiz.TABLE_NAME, null, null, null, null, null, null, null);
        List<QuizModel> quizList = new ArrayList<>();
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                QuizModel quiz = getQuiz(c);
                quizList.add(quiz);
                c.moveToNext();
            }
        }
        c.close();
        db.close();
        return quizList;
    }

    public List<QuizModel> getRandomQuizList(int limit) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM " + DBSchema.Quiz.TABLE_NAME + " ORDER BY RANDOM() LIMIT "
                + limit, null);
        List<QuizModel> quizList = new ArrayList<>();
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                QuizModel quiz = getQuiz(c);
                quizList.add(quiz);
                c.moveToNext();
            }
        }
        c.close();
        db.close();
        return quizList;
    }

    public QuizModel getQuiz(Cursor c) {
        String question = c.getString(c.getColumnIndex(DBSchema.Quiz.COLUMN_QUESTION));
        String firstAnswer = c.getString(c.getColumnIndex(DBSchema.Quiz.COLUMN_FIRST_ANSWER));
        String secondAnswer = c.getString(c.getColumnIndex(DBSchema.Quiz.COLUMN_SECOND_ANSWER));
        String thirdAnswer = c.getString(c.getColumnIndex(DBSchema.Quiz.COLUMN_THIRD_ANSWER));
        String fourthAnswer = c.getString(c.getColumnIndex(DBSchema.Quiz.COLUMN_FOURTH_ANSWER));
        int rightAnswer = c.getInt(c.getColumnIndex(DBSchema.Quiz.COLUMN_RIGHT_ANSWER));
        long id = c.getLong(c.getColumnIndex(DBSchema.Quiz._ID));

        QuizModel quiz = new QuizModel();
        quiz.setQuestion(question);
        quiz.setFirstAnswer(firstAnswer);
        quiz.setSecondAnswer(secondAnswer);
        quiz.setThirdAnswer(thirdAnswer);
        quiz.setFourthAnswer(fourthAnswer);
        quiz.setRightAnswer(rightAnswer);
        quiz.setId(id);

        return quiz;
    }
}
package com.example.hp.quiz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<QuizModel> items;

    public CustomAdapter(Context context) {
        items = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public QuizModel getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_item, parent, false);
        }
        TextView tvQuizId = (TextView) convertView.findViewById(R.id.tv_quiz_id);
        TextView tvQuizQuestion = (TextView) convertView.findViewById(R.id.tv_quiz_question);

        QuizModel quizModel = getItem(position);

        tvQuizId.setText("" + quizModel.getId());
        tvQuizQuestion.setText(quizModel.getQuestion());

        return convertView;
    }

    public List<QuizModel> getItems() {
        return items;
    }

    public void setItems(List<QuizModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
package com.example.hp.quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddQuizActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etQuestion;
    private EditText etFirstAnswer;
    private EditText etSecondAnswer;
    private EditText etThirdAnswer;
    private EditText etFourthAnswer;
    private EditText etRightAnswer;
    private Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_quiz);

        findViews();
        btnAdd.setOnClickListener(this);
    }

    public void findViews() {
        etQuestion = (EditText) findViewById(R.id.et_question);
        etFirstAnswer = (EditText) findViewById(R.id.et_first_question);
        etSecondAnswer = (EditText) findViewById(R.id.et_second_question);
        etThirdAnswer = (EditText) findViewById(R.id.et_third_question);
        etFourthAnswer = (EditText) findViewById(R.id.et_fourth_question);
        etRightAnswer = (EditText) findViewById(R.id.et_right_answer);
        btnAdd = (Button) findViewById(R.id.btn_add);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_add) {
            if (isEmpty(etQuestion)) {
                Toast.makeText(AddQuizActivity.this, "Question must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etFirstAnswer)) {
                Toast.makeText(AddQuizActivity.this, "First answer must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etSecondAnswer)) {
                Toast.makeText(AddQuizActivity.this, "Second answer must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etThirdAnswer)) {
                Toast.makeText(AddQuizActivity.this, "Third answer must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etFourthAnswer)) {
                Toast.makeText(AddQuizActivity.this, "Fourth answer must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etRightAnswer)) {
                Toast.makeText(AddQuizActivity.this, "Right answer must not be empty", Toast.LENGTH_SHORT).show();
            } else {
                String question = etQuestion.getText().toString();
                String firstAnswer = etFirstAnswer.getText().toString();
                String secondAnswer = etSecondAnswer.getText().toString();
                String thirdAnswer = etThirdAnswer.getText().toString();
                String fourthAnswer = etFourthAnswer.getText().toString();
                int rightAnswer = Integer.parseInt(etRightAnswer.getText().toString());

                QuizModel quizModel = new QuizModel();
                quizModel.setQuestion(question);
                quizModel.setFirstAnswer(firstAnswer);
                quizModel.setSecondAnswer(secondAnswer);
                quizModel.setThirdAnswer(thirdAnswer);
                quizModel.setFourthAnswer(fourthAnswer);
                if(rightAnswer < 1 || rightAnswer > 4) {
                    Toast.makeText(AddQuizActivity.this, "Right answer must be from 1 to 4", Toast.LENGTH_SHORT).show();
                } else {
                    quizModel.setRightAnswer(rightAnswer);

                    long rowId = DBHelper.getInstance(this).addQuiz(quizModel);


                    if (rowId <= 0) {
                        Toast.makeText(AddQuizActivity.this, "Could not add quiz", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddQuizActivity.this, "Added", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        }
    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() <= 0;
    }
}
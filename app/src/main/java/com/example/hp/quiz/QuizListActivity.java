package com.example.hp.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class QuizListActivity extends AppCompatActivity {

    private CustomAdapter customAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_list);

        ListView listView = (ListView) findViewById(R.id.listView);
        customAdapter = new CustomAdapter(this);
        listView.setAdapter(customAdapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(QuizListActivity.this, QuizModifyActivity.class);
                intent.putExtra("id", customAdapter.getItem(position).getId() + "");
                startActivity(intent);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        List<QuizModel> quizList = DBHelper.getInstance(this).getQuizList();
        customAdapter.setItems(quizList);
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.quiz_list_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_add) {
            Intent intent = new Intent(this, AddQuizActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


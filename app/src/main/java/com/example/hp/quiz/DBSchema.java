package com.example.hp.quiz;

import android.provider.BaseColumns;

public final class DBSchema {

    public static final class Quiz implements BaseColumns {

        public static final String TABLE_NAME = "quiz";

        public static final String COLUMN_QUESTION = "question";
        public static final String COLUMN_FIRST_ANSWER = "first_answer";
        public static final String COLUMN_SECOND_ANSWER = "second_answer";
        public static final String COLUMN_THIRD_ANSWER = "third_answer";
        public static final String COLUMN_FOURTH_ANSWER = "fourth_answer";
        public static final String COLUMN_RIGHT_ANSWER = "right_answer";

        public static final String CREATE_TABLE_SQL = "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_QUESTION + " TEXT, " +
                COLUMN_FIRST_ANSWER + " TEXT, " +
                COLUMN_SECOND_ANSWER + " TEXT, " +
                COLUMN_THIRD_ANSWER + " TEXT, " +
                COLUMN_FOURTH_ANSWER + " TEXT, " +
                COLUMN_RIGHT_ANSWER + " INTEGER);";

//        public static final String DROP_TABLE_SQL = "DROP TABLE IF IT EXISTS " + TABLE_NAME;
    }

}

package com.example.hp.quiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnStart;
    private Button btnTop;
    private Button btnSettings;
    private Button btnAbout;
    private Button btnExit;
    public SharedPreferences sPref;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    int score = data.getIntExtra(Constants.KEY_SCORE, 0);
                    String name = data.getStringExtra(Constants.KEY_NAME);
                    updateTop(score, name);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkFirstRun();
        findViews();
        setClickListeners();
    }

    public void findViews() {

        btnStart = (Button) findViewById(R.id.btn_start);
        btnSettings = (Button) findViewById(R.id.btn_settings);
        btnAbout = (Button) findViewById(R.id.btn_about);
        btnTop = (Button) findViewById(R.id.btn_top);
        btnExit = (Button) findViewById(R.id.btn_exit);
    }

    public void setClickListeners() {

        btnStart.setOnClickListener(this);
        btnTop.setOnClickListener(this);
        btnSettings.setOnClickListener(this);
        btnAbout.setOnClickListener(this);
        btnExit.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_edit) {
            Intent intent = new Intent(this, QuizListActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_start:

                Intent intent = new Intent(this, QuizActivity.class);
                intent.putExtra(Constants.KEY_TOP_SCORE, lestTop());
                startActivityForResult(intent, 111);

                break;

            case R.id.btn_top:

                TopDialog topDialog = new TopDialog();
                Bundle bundle = new Bundle();
                sendDataTop(bundle);
                topDialog.setArguments(bundle);
                topDialog.show(getSupportFragmentManager(), null);

                break;

            case R.id.btn_settings:

                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                settingsIntent.putExtra(Constants.KEY_TOP_SCORE, lestTop());
                startActivityForResult(settingsIntent, 111);

                break;

            case R.id.btn_about:

                new AlertDialog.Builder(this
                )
                        .setTitle("About Program")
                        .setMessage("Correct Java for Android version 1.0.0  " +
                                " The Authors of the program R.H. , H.M. , N.G, E.J." +
                                "          2016        ")
                        .setNegativeButton("Ok", null).
                        setCancelable(false).create().show();
                break;


            case R.id.btn_exit:
                new AlertDialog.Builder(this)
                        .setTitle("EXIT")
                        .setMessage("Do you really want to exit?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).setIcon(R.drawable.min_exit)
                        .setNegativeButton("Cancel", null)
                        .setCancelable(false).create().show();
        }
    }

    public void checkFirstRun() {
        while (!getSharedPreferences("PREFERENCE", 0).getBoolean("isFirstRun", true)) {
            return;
        }

        for (int i = 1; i <= 10; i++) {
            setSaveData(Constants.KEY_SAVE_SCORE + i, 0);
            setSaveData(Constants.KEY_SAVE_NAME + i, "");
        }
        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("isFirstRun", false).apply();
    }


    public int getSaveDataInt(String paramString) {

        sPref = getSharedPreferences(paramString, 0);
        return sPref.getInt(paramString, 0);
    }


    public String getSaveDataString(String paramString) {

        sPref = getSharedPreferences(paramString, MODE_PRIVATE);
        return sPref.getString(paramString, "");
    }


    public void setSaveData(String name, String data) {

        sPref = getSharedPreferences(name, MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString(name, data);
        editor.commit();
    }

    public void setSaveData(String name, int data) {

        sPref = getSharedPreferences(name, MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt(name, data);
        editor.commit();
    }

    public void updateTop(int score, String name) {
        int i;
        for (i = 1; i < 11; i++) {

            if (score > getSaveDataInt(Constants.KEY_SAVE_SCORE + i)) {
                setSaveData(Constants.KEY_SAVE_SCORE + 11, score);
                setSaveData(Constants.KEY_SAVE_NAME + 11, name);
                for (int j = i; j <= 11; j++) {
                    int k = getSaveDataInt(Constants.KEY_SAVE_SCORE + j);
                    String str = getSaveDataString(Constants.KEY_SAVE_NAME + j);
                    setSaveData(Constants.KEY_SAVE_SCORE + j, getSaveDataInt(Constants.KEY_SAVE_SCORE + 11));
                    setSaveData(Constants.KEY_SAVE_NAME + j, getSaveDataString(Constants.KEY_SAVE_NAME + 11));
                    setSaveData(Constants.KEY_SAVE_SCORE + 11, k);
                    setSaveData(Constants.KEY_SAVE_NAME + 11, str);
                }
                break;
            }
        }
    }

    public void sendDataTop(Bundle bundle) {

        for (int i = 1; i <= 10; i++) {

            if (getSaveDataInt(Constants.KEY_SAVE_SCORE + i) == 0) {
                if (i == 1) {
                    bundle.putInt(Constants.KEY_QUANTITY, 0);
                    break;
                } else {
                    bundle.putInt(Constants.KEY_QUANTITY, i - 1);
                    break;
                }

            }
            bundle.putInt(Constants.KEY_QUANTITY, i);
            bundle.putInt(Constants.KEY_SAVE_SCORE1 + i, getSaveDataInt(Constants.KEY_SAVE_SCORE + i));
            bundle.putString(Constants.KEY_SAVE_NAME1 + i, getSaveDataString(Constants.KEY_SAVE_NAME + i));
        }

    }

    public int lestTop() {
        int i = 1;
        while (getSaveDataInt(Constants.KEY_SAVE_SCORE + i) != 0 && i <= 10) {
            i++;
        }
        return getSaveDataInt(Constants.KEY_SAVE_SCORE + i);
    }
}
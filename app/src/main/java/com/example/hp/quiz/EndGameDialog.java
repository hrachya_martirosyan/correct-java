package com.example.hp.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EndGameDialog extends DialogFragment implements View.OnClickListener {

    public Intent intent;
    private EditText name;
    private int visibleFlag;

    public EndGameDialog() {
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        int score = getArguments().getInt(Constants.KEY_SCORE_FOR_DIALOG);
        int topScore = getActivity().getIntent().getIntExtra(Constants.KEY_TOP_SCORE, 0);
        setCancelable(false);
        intent = new Intent(getActivity(), MainActivity.class);
        View localView = paramLayoutInflater.inflate(R.layout.dialog_end_game, null);
        ((TextView) localView.findViewById(R.id.tv_score_game_over)).setText("" + score);
        name = ((EditText) localView.findViewById(R.id.et_name));
        TextView title = (TextView) localView.findViewById(R.id.end_game_title);
        name.setVisibility(View.INVISIBLE);
        if (score > topScore) {
            intent.putExtra(Constants.KEY_SCORE, score);
            name.setVisibility(View.VISIBLE);
            title.setText("   You Win   ");
            visibleFlag = 1;
        } else {
            title.setText("   You Lose   ");
        }

        Button btnBack = (Button) localView.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        return localView;
    }

    @Override
    public void onClick(View v) {

        if (visibleFlag == 1) {
            intent.putExtra(Constants.KEY_NAME, name.getText().toString());
        }
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
        dismiss();
    }
}

package com.example.hp.quiz;

public class Constants {

    public static final int QUANTITY_OF_QUESTIONS = 20;
    public static final int SPLASH_DISPLAY_LENGTH = 1000;
    public static final int TIMER = 90;

    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_QUANTITY = "KEY_QUANTITY";
    public static final String KEY_SAVE_NAME = "Save_Name";
    public static final String KEY_SAVE_NAME1 = "Save_Name1";
    public static final String KEY_SAVE_SCORE = "Save_Score";
    public static final String KEY_SAVE_SCORE1 = "Save_Score1";
    public static final String KEY_SCORE = "KEY_SCORE";
    public static final String KEY_TOP_SCORE = "Save_Top";

    public static final String KEY_SCORE_FOR_DIALOG = "KEY_SCORE_FOR_DIALOG";

}

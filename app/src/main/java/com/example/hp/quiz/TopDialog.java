package com.example.hp.quiz;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class TopDialog extends DialogFragment implements AdapterView.OnClickListener {

    public CustomAdapterTop customAdapterTop;

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View localView = paramLayoutInflater.inflate(R.layout.dialog_top, null);
        setCancelable(false);
        ListView listView = (ListView) localView.findViewById(R.id.lv_top);
        Button btnBack = (Button) localView.findViewById(R.id.btn_top_back);
        btnBack.setOnClickListener(this);
        ArrayList items = new ArrayList();
        int i = getArguments().getInt(Constants.KEY_QUANTITY);
        for (int j = 1; j <= i; j++) {
            Contact contact = new Contact(j, getArguments().getString(Constants.KEY_SAVE_NAME1 + j), getArguments().getInt(Constants.KEY_SAVE_SCORE1 + j));
            items.add(contact);
        }
        customAdapterTop = new CustomAdapterTop(getActivity(), items);
        listView.setAdapter(customAdapterTop);
        return localView;
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}

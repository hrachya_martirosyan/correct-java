package com.example.hp.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class QuizModifyActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etModifyQuestion;
    private EditText etModifyFirstAnswer;
    private EditText etModifySecondAnswer;
    private EditText etModifyThirdAnswer;
    private EditText etModifyFourthAnswer;
    private EditText etModifyRightAnswer;
    private Button btnModify;
    private Button btnDelete;
    QuizModel quizModel;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_modify);

        Intent intent = getIntent();
        id = Integer.parseInt(intent.getStringExtra("id"));

//        DBHelper dbHelper = DBHelper.getInstance(this);
        quizModel = new QuizModel();
        quizModel = DBHelper.getInstance(this).getQuizById(id);

        findViews();
        setText();

        btnModify.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
    }

    public void findViews() {
        etModifyQuestion = (EditText) findViewById(R.id.et_modify_question);
        etModifyFirstAnswer = (EditText) findViewById(R.id.et_modify_first_question);
        etModifySecondAnswer = (EditText) findViewById(R.id.et_modify_second_question);
        etModifyThirdAnswer = (EditText) findViewById(R.id.et_modify_third_question);
        etModifyFourthAnswer = (EditText) findViewById(R.id.et_modify_fourth_question);
        etModifyRightAnswer = (EditText) findViewById(R.id.et_modify_right_answer);
        btnModify = (Button) findViewById(R.id.btn_modify);
        btnDelete = (Button) findViewById(R.id.btn_delete);

    }

    public void setText() {
        etModifyQuestion.setText(quizModel.getQuestion());
        etModifyFirstAnswer.setText(quizModel.getFirstAnswer());
        etModifySecondAnswer.setText(quizModel.getSecondAnswer());
        etModifyThirdAnswer.setText(quizModel.getThirdAnswer());
        etModifyFourthAnswer.setText(quizModel.getFourthAnswer());
        etModifyRightAnswer.setText("" + quizModel.getRightAnswer());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_modify) {

            if (isEmpty(etModifyQuestion)) {
                Toast.makeText(QuizModifyActivity.this, "Question must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etModifyFirstAnswer)) {
                Toast.makeText(QuizModifyActivity.this, "First answer must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etModifySecondAnswer)) {
                Toast.makeText(QuizModifyActivity.this, "Second answer must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etModifyThirdAnswer)) {
                Toast.makeText(QuizModifyActivity.this, "Third answer must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etModifyFourthAnswer)) {
                Toast.makeText(QuizModifyActivity.this, "Fourth answer must not be empty", Toast.LENGTH_SHORT).show();
            } else if (isEmpty(etModifyRightAnswer)) {
                Toast.makeText(QuizModifyActivity.this, "Right answer must not be empty", Toast.LENGTH_SHORT).show();
            } else {
                String question = etModifyQuestion.getText().toString();
                String firstAnswer = etModifyFirstAnswer.getText().toString();
                String secondAnswer = etModifySecondAnswer.getText().toString();
                String thirdAnswer = etModifyThirdAnswer.getText().toString();
                String fourthAnswer = etModifyFourthAnswer.getText().toString();
                int rightAnswer = Integer.parseInt(etModifyRightAnswer.getText().toString());

                QuizModel quizModel = new QuizModel();
                quizModel.setQuestion(question);
                quizModel.setFirstAnswer(firstAnswer);
                quizModel.setSecondAnswer(secondAnswer);
                quizModel.setThirdAnswer(thirdAnswer);
                quizModel.setFourthAnswer(fourthAnswer);
                if (rightAnswer < 1 || rightAnswer > 4) {
                    Toast.makeText(QuizModifyActivity.this, "Right answer must be from 1 to 4", Toast.LENGTH_SHORT).show();
                } else {
                    quizModel.setRightAnswer(rightAnswer);
                    quizModel.setId(id);

                    long rowId = DBHelper.getInstance(this).updateQuiz(quizModel);

                    if (rowId <= 0) {
                        Toast.makeText(QuizModifyActivity.this, "Could not modify quiz", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(QuizModifyActivity.this, "Modified", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        }
        if (v.getId() == R.id.btn_delete) {

            long rowId = DBHelper.getInstance(this).deleteQuiz(quizModel);

            if (rowId <= 0) {
                Toast.makeText(QuizModifyActivity.this, "Could not delete quiz", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(QuizModifyActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() <= 0;
    }

}

package com.example.hp.quiz;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvQuestion;
    private List<QuizModel> quizModel;
    private Timer timer;
    private int index = 0;
    private int score = 0;
    private int clickProgressBar = 5;
    private int seconds = Constants.TIMER;

    private boolean running = true;

    private RadioGroup rgAnswers;
    private RadioButton rbFirstAnswer;
    private RadioButton rbSecondAnswer;
    private RadioButton rbThirdAnswer;
    private RadioButton rbFourthAnswer;
    private int selectedAnswer;
    private Button btnCheck;

    private ProgressBar progressBar;

    private ImageView imgMinionAnimation;
    private ImageView imgAnswer;
    private SoundPool soundPool;
    int wronganswer;

    int rigthanswer;
    private AssetManager assets;
    private int mStreamID;
    private Button btnFinish;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            createOldSoundPool();
        } else {
            createNewSoundPool();
        }
        assets = getAssets();

        wronganswer = loadSound("wronganswer.mp3");
        rigthanswer = loadSound("rigthanswer.mp3");

        findViews();
        setClickListeners();
        runTimer();








        imgMinionAnimation = (ImageView) findViewById(R.id.imgMinionAsk);
        imgMinionAnimation.setBackgroundResource(R.drawable.minion_answer);

        AnimationDrawable trueMin = (AnimationDrawable) imgMinionAnimation.getBackground();
        trueMin.setOneShot(false);
        trueMin.start();

        imgAnswer = (ImageView) findViewById(R.id.img_answer);
















        if (running) {

            btnCheck.setBackground(getResources().getDrawable(R.drawable.buttonshape1));
            imgMinionAnimation.setVisibility(View.INVISIBLE);
            imgMinionAnimation.setBackgroundResource(R.drawable.minion_answer);
            imgMinionAnimation.setVisibility(View.VISIBLE);
        }

        progressBar.setProgress(clickProgressBar);



        rgAnswers.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                btnCheck.setEnabled(true);

                switch (checkedId) {
                    case R.id.rb_first_answer:
                        selectedAnswer = 1;
                        break;
                    case R.id.rb_second_answer:
                        selectedAnswer = 2;
                        break;
                    case R.id.rb_third_answer:
                        selectedAnswer = 3;
                        break;
                    case R.id.rb_fourth_answer:
                        selectedAnswer = 4;
                        break;
                }
            }
        });




        DBHelper dbHelper = DBHelper.getInstance(this);

        quizModel = new ArrayList<>();
        quizModel = dbHelper.getRandomQuizList(Constants.QUANTITY_OF_QUESTIONS);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor c = db.query(DBSchema.Quiz.TABLE_NAME, null, null, null, null, null, null, null);

        c.close();
        dbHelper.close();

        setText();
    }

    private void findViews() {

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnCheck = (Button) findViewById(R.id.btn_check);
        btnFinish = (Button) findViewById(R.id.btn_finish);

        tvQuestion = (TextView) findViewById(R.id.tv_question);

        rgAnswers = (RadioGroup) findViewById(R.id.rg_answers);

        rbFirstAnswer = (RadioButton) findViewById(R.id.rb_first_answer);
        rbSecondAnswer = (RadioButton) findViewById(R.id.rb_second_answer);
        rbThirdAnswer = (RadioButton) findViewById(R.id.rb_third_answer);
        rbFourthAnswer = (RadioButton) findViewById(R.id.rb_fourth_answer);


    }

    private void setClickListeners() {

        btnCheck.setOnClickListener(this);
        btnFinish.setOnClickListener(this);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void createNewSoundPool() {
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        soundPool = new SoundPool.Builder()
                .setAudioAttributes(attributes)
                .build();
    }

    @SuppressWarnings("deprecation")
    private void createOldSoundPool() {
        soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
    }

    private int playSound(int sound) {
        if (sound > 0) {
            mStreamID = soundPool.play(sound, 1, 1, 1, 0, 1);
        }
        return mStreamID;
    }

    private int loadSound(String fileName) {
        AssetFileDescriptor afd;
        try {
            afd = assets.openFd(fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
        return soundPool.load(afd, 1);
    }

    public void setText() {
        tvQuestion.setText(quizModel.get(index).getQuestion());
        rbFirstAnswer.setText(quizModel.get(index).getFirstAnswer());
        rbSecondAnswer.setText(quizModel.get(index).getSecondAnswer());
        rbThirdAnswer.setText(quizModel.get(index).getThirdAnswer());
        rbFourthAnswer.setText(quizModel.get(index).getFourthAnswer());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_check:

                String buttonText = btnCheck.getText().toString();

                if (buttonText.equals("Check")) {

                    if (selectedAnswer == quizModel.get(index).getRightAnswer()) {
                        imgMinionAnimation.setVisibility(View.INVISIBLE);
                        imgAnswer.setVisibility(View.VISIBLE);
                        imgAnswer.setImageResource(R.drawable.button_true);
                        playSound(rigthanswer);
                        imgMinionAnimation.setBackgroundResource(R.drawable.minion_true);
                        imgMinionAnimation.setVisibility(View.VISIBLE);
                        score++;

                    } else {
                        imgMinionAnimation.setVisibility(View.INVISIBLE);
                        imgAnswer.setVisibility(View.VISIBLE);
                        imgAnswer.setImageResource(R.drawable.button_false);
                        playSound(wronganswer);
                        imgMinionAnimation.setBackgroundResource(R.drawable.minion_false);
                        imgMinionAnimation.setVisibility(View.VISIBLE);
//                        score++;
                    }
                    btnCheck.setText("Next");


                } else {
                    clickProgressBar = clickProgressBar + 5;
                    if (progressBar.getProgress() <= 100) {
                        progressBar.setProgress(clickProgressBar);
                    }
                    if (index < quizModel.size() - 1) {
                        index++;
                        setText();
                    } else {
                        endGame();
                    }
                    rgAnswers.clearCheck();
                    imgMinionAnimation.setVisibility(View.INVISIBLE);
                    imgMinionAnimation.setBackgroundResource(R.drawable.minion_answer);
                    imgMinionAnimation.setVisibility(View.VISIBLE);


                    btnCheck.setVisibility(View.VISIBLE);
                    btnCheck.setText("Check");
                    btnCheck.setEnabled(false);
                    imgAnswer.setVisibility(View.INVISIBLE);
                }

                break;

            case R.id.btn_finish:
                endGame();
//                Intent returnIntent = new Intent();
//                returnIntent.putExtra("result", score);
//                setResult(RESULT_OK, returnIntent);
//                finish();
                break;
        }
    }

    public void timerStart() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (!running) {
                            timer.cancel();


                            if (seconds == 0) {
                                endGame();
                            }
                        } else {

                            running = true;
                        }
                    }
                });
            }
        }, 1, Constants.TIMER);
    }

    private void runTimer() {
        final TextView timeView = (TextView) findViewById(R.id.time_view);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {

                int minutes = (seconds % 3600) / 60;
                int secs = seconds % 60;
                String time = String.format("  %02d:%02d", minutes, secs);
                timeView.setText(time);
                if (running) {
                    if (seconds != 0) {
                        seconds--;
                    } else {
                        running = false;
                    }
                } else {
//                    endGame();
                }
                handler.postDelayed(this, 1000);
            }
        });
    }

    public void endGame() {

        Bundle bundle = new Bundle();
        bundle.putInt(Constants.KEY_SCORE_FOR_DIALOG, score);
        EndGameDialog endGameDialog = new EndGameDialog();
        endGameDialog.setArguments(bundle);
        endGameDialog.show(getSupportFragmentManager(), null);
    }
}